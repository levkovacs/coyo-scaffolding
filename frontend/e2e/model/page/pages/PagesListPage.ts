import {CoyoPage} from '../CoyoPage';

export class PagesListPage extends CoyoPage {

    constructor() {
        super('/pages');
    }
}
