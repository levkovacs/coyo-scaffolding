(function () {
  'use strict';

  var NavigationPage = require('../navigation.page.js');
  var Search = require('./search.page.js');
  var PageDetails = require('./../pages/page-details.page.js');
  var login = require('../../login.page.js');
  var testhelper = require('../../../testhelper.js');

  describe('global search', function () {
    var navigation, search, pageDetails;

    beforeAll(function () {
      navigation = new NavigationPage();
      search = new Search();
      pageDetails = new PageDetails();
      login.loginDefaultUser();
    });

    beforeEach(function () {
      navigation.getHome();
    });

    afterAll(function () {
      testhelper.deletePages();
    });

    // Only known pages are possible due to the unknown indexing time
    it('search for page', function () {
      var pageName = 'About Coyo';
      // search for page
      navigation.searchIcon.click();
      navigation.search.searchInput(pageName);
      testhelper.cancelTour();

      var filterElem = search.filter.page;

      // filter page
      filterElem.click();
      search.searchresults.itemByIndex(0).then(function (item) {
        item.click();
      });

      expect(pageDetails.title.getText()).toBe(pageName);
    });

    it('search for wiki article', function () {
      var article = 'About';

      // search for article
      navigation.searchIcon.click();
      navigation.search.searchInput(article);
      testhelper.cancelTour();

      var filterElem = search.filter.wikiArticle;

      // filter wiki article
      filterElem.click();
      expect(search.searchresults.items.count()).toBeGreaterThan(2);
    });

    // Only known pages are possible due to the unknown indexing time
    it('search for name ', function () {
      var pageName = 'Company News';
      navigation.searchIcon.click();
      navigation.search.searchInput(pageName);
      testhelper.cancelTour();
      // filter page
      search.filter.page.click();

      var resultElem = search.searchresults.itemByName(pageName);

      resultElem.click();

      expect(pageDetails.title.getText()).toBe(pageName);
    });

  });
})();
