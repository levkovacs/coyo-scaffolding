(function () {
  'use strict';

  var webdriver = require('selenium-webdriver');
  var Navigation = require('../navigation.page.js');
  var WorkspaceList = require('./workspace-list.page.js');
  var WorkspaceCreate = require('./workspace-create.page.js');
  var WorkspaceDetails = require('./workspace-details.page.js');
  var Pagination = require('../../pagination.page.js');
  var login = require('../../login.page.js');
  var component = require('../../components.page.js');
  var testhelper = require('../../../testhelper.js');

  describe('workspaces', function () {
    var navigation, workspaceList, workspaceCreate, workspaceDetails, pagination;
    var PAGE_SIZE = 20;

    beforeAll(function () {
      navigation = new Navigation();
      workspaceList = new WorkspaceList();
      workspaceCreate = new WorkspaceCreate();
      workspaceDetails = new WorkspaceDetails();
      pagination = new Pagination();
      login.loginDefaultUser();
    });

    beforeEach(function () {
      navigation.getHome();
    });

    afterAll(function () {
      testhelper.deleteWorkspace();
    });

    it('create and delete a workspace', function () {
      var key = Math.floor(Math.random() * 1000000);
      var workspaceName = 'createTestWorkspace' + key;
      // create workspace
      navigation.workspaces.click();
      testhelper.cancelTour();

      workspaceList.newButton.click();
      workspaceCreate.page1.name.sendKeys(workspaceName);
      workspaceCreate.page1.continueButton.click();

      workspaceCreate.page2.continueButton.click();

      expect(workspaceDetails.title.getText()).toBe(workspaceName);

      // delete workspace
      workspaceDetails.options.settings.click();
      workspaceDetails.settings.deleteButton.click();
      component.modals.confirm.deleteButton.click();

      // search if workspace is present
      workspaceList.filter.all.click();
      expect(workspaceList.search(workspaceName).count()).toBe(0);

      // clear search field
      workspaceList.searchInput.clear();
      expect(workspaceList.searchInput.getText()).toBe('');
    });

    it('navigate to workspace', function () {
      // create a new workspace
      var key = Math.floor(Math.random() * 1000000);
      var workspaceName = 'createTestWorkspace' + key;
      testhelper.createWorkspace(workspaceName, 'PUBLIC');

      navigation.workspaces.click();
      testhelper.cancelTour();

      // search for workspace
      workspaceList.search(workspaceName).then(function (list) {
        expect(list.length).toBe(1);
        workspaceList.navigateTo(list[0]);
      });

      expect(workspaceDetails.title.getText()).toBe(workspaceName);

      // clear search field
      navigation.workspaces.click();
      workspaceList.searchInput.clear();
      expect(workspaceList.searchInput.getText()).toBe('');
    });

    it('search workspace with multiple categories', function () {
      var categoryProjectsName = 'Projects';
      var categoryIdeasName = 'Ideas';
      var key = Math.floor(Math.random() * 1000000);
      var workspaceName = 'workspacename_' + key;
      var workspaceDescription = 'workspacedescription_' + key;
      var categories = [];
      var categoryModel = 'WorkspaceCategoryModel';
      testhelper.getCategoryId(categoryProjectsName, categoryModel).then(function (categoryId) {
        categories.push({categoryName: categoryProjectsName, categoryId: categoryId});
        testhelper.getCategoryId(categoryIdeasName, categoryModel).then(function (categoryId) {
          categories.push({categoryName: categoryIdeasName, categoryId: categoryId});
          testhelper.createWorkspaceWithMultipleCategories(workspaceName, workspaceDescription, categories, 'PUBLIC',
              function () {
                navigation.workspaces.click();
                testhelper.cancelTour();

                workspaceList.filterCategory(categories[0].categoryName);
                workspaceList.filterCategory(categories[1].categoryName);

                expect(workspaceList.list.workspaceList.count()).toBe(3);
              });
        });
      });
    });

    it('pagination', function () {
      var promises = [];
      for (var i = 0; i < PAGE_SIZE + 1; i++) {
        var key = Math.floor(Math.random() * 1000000);
        var workspaceName = 'createTestWorkspacePagination' + key;
        promises.push(testhelper.createWorkspace(workspaceName, 'PUBLIC'));
      }

      webdriver.promise.all(promises).then(function () {
        navigation.workspaces.click();
        testhelper.cancelTour();
        expect(workspaceList.workspaces.count()).toBe(20);

        pagination.nextPage();
        expect(pagination.activePage().getText()).toBe('2');
      });
    });
  });

})();
