(function () {
  'use strict';

  function Notification() {
    var api = this;

    api.notificationPage = {
      timelineItem: $('.timeline-item-body'),
      comments: $$('.comment-message')
    };
  }

  module.exports = Notification;

})();
