(function () {
  'use strict';

  var login = require('../../../login.page.js');
  var Navigation = require('../../navigation.page');
  var WidgetSlot = require('../widget-slot.page');
  var HeadlineWidget = require('./headline-widget.page');
  var components = require('../../../components.page');
  var testhelper = require('../../../../testhelper');

  describe('headline widget', function () {
    var widgetSlot, widgetChooser, widget, headlineWidget, navigation;

    beforeAll(function () {
      login.loginDefaultUser();
      testhelper.createAndOpenPage('testpage' + Math.floor(Math.random() * 1000000));
    });

    afterAll(function () {
      testhelper.deletePages();
    });

    it('should create a headline widget', function () {
      // add a new headline widget
      navigation = new Navigation();
      navigation.editView();
      widgetSlot = new WidgetSlot('page-sidebar-bottom');
      widgetChooser = widgetSlot.widgetChooser;
      widget = widgetSlot.getWidget(0);
      headlineWidget = new HeadlineWidget(widget);

      widgetSlot.addButton.click();
      widgetChooser.selectByName('Headline');

      // set the headline text
      headlineWidget.headline.click();
      headlineWidget.headline.sendKeys('Widget Headline');
      navigation.viewEditOptions.saveButton.click();

      expect(headlineWidget.headlineHasClass('h2')).toBe(true);
      expect(headlineWidget.headline.getText()).toBe('Widget Headline');

      // modify text and size
      navigation.editView();
      headlineWidget.hover();
      headlineWidget.inlineOptions.sizeToggle.click();
      headlineWidget.inlineOptions.sizeOption('XL').click();
      headlineWidget.headline.click();
      headlineWidget.headline.clear();
      headlineWidget.headline.sendKeys('My Widget Headline');
      navigation.viewEditOptions.saveButton.click();

      expect(headlineWidget.headlineHasClass('h1')).toBe(true);
      expect(headlineWidget.headlineHasClass('h2')).toBe(false);
      expect(headlineWidget.headline.getText()).toBe('My Widget Headline');

      // remove the widget
      navigation.editView();
      headlineWidget.hover();
      headlineWidget.removeButton.click();
      components.modals.confirm.deleteButton.click();
      navigation.viewEditOptions.saveButton.click();

      expect(widgetSlot.allWidgets.count()).toBe(0);
    });
  });
})();
