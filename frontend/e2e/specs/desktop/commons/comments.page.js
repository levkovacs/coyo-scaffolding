(function () {
  'use strict';

  var Comment = require('./comment.page');

  function Comments(commentsElement) {
    var api = this,
        commentForm = commentsElement.$('.comments-form-inner');

    api.element = commentsElement;
    api.comments = Comment.getAllElements(api.element);
    api.form = {
      textarea: commentForm.$('textarea[ng-model="$ctrl.formModel.message"]'),
      emoji: commentForm.$('.emoji-picker-toggle'),
      attachment: commentForm.$('a[ngf-select="$ctrl.addAttachments($files, $invalidFiles)"]'),
      submitBtn: commentForm.$('button[label="SEND"]')
    };
    api.getComment = function (index) {
      return Comment.create(api.element, index);
    };
    api.addComment = function (message) {
      api.form.textarea.sendKeys(message);
      api.form.submitBtn.click();
      return api.comments.count().then(function (number) {
        var latestComment = new Comment(api.comments.get(number - 1));
        expect(latestComment.message.getText()).toBe(message);
        return latestComment;
      });
    };
  }

  Comments.getElement = function (context) {
    return context.$('coyo-comments .comments');
  };

  Comments.create = function (context) {
    return new Comments(Comments.getElement(context));
  };

  module.exports = Comments;

})();
